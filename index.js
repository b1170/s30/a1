const express = require('express');
const app = express();
const PORT = 3000;
const mongoose = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://adminDB:FhXqMxMv8TqbV7@batch139.ndmwa.mongodb.net/user', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Successfully connected to database`));

//Schema

const userSchema = new mongoose.Schema({
    name: String,
    password:String
})

const userCreate = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
    let newUser = new userCreate({name: req.body.name, password: req.body.password}) 

    newUser.save((err, result) => {
        if(err) {
            console.log(`There is an error`);
        } else {
            return res.send(newUser);
        }
    });
});

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));